# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'f5877d5cc96fbf4c04ae49430861fed8bf4812de081268d2e5a7aa2672c3404d3a7aec693043eeb80dd376252236af93fac9c9846a94e39c4910c269815cee47'
